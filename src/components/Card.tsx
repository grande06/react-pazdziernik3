import * as React from "react";
import { StatelessComponent } from "react";

type Props = {
  title: string;
};

export const Card: StatelessComponent<Props> = props => {
  return (
    <div className="card">
      <div className="card-body">
        <h5 className="card-title">{props.title}</h5>
        {props.children}
      </div>
    </div>
  );
};
