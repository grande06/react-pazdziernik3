import { StatelessComponent } from "react";
import * as React from "react";
import { Album } from "../model/Album";
import "./SearchResults.css";

type Props = {
  results: Album[];
  loading?: boolean;
  error?: string;
};

export const SearchResults: StatelessComponent<Props> = props => (
  <div className="search-results">
    {props.loading && <p>Please wait loading results ...</p>}
    {props.error && <p>Error: {props.error}</p>}

    <div className="card-group">
      {props.results.map(item => (
        <div className="card" key={item.id}>
          <img className="card-img-top" src={item.images[0].url} />
          <div className="card-body">
            <h5 className="card-title">{item.name}</h5>
          </div>
        </div>
      ))}
    </div>
  </div>
);
